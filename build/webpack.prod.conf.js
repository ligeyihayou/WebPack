// 该文件的配置全部适用于线上发布
const utils = require('./utils')
const config = require('../config')
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const baseWebpackConfig = require('./webpack.base.conf')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// 合并webpack配置文件，类似于 Object.assign
const webpackConfig = merge(baseWebpackConfig, {
    // 这里可以进行添加和覆盖 baseWebpackConfig 里面的配置
    devtool: '#source-map',
    // 重新调整输出的路径
    output: {
        path: config.build.assetsRoot,
        filename: utils.assetsPath('js/[name].[chunkhash].js'),
        chunkFilename: utils.assetsPath('js/[id].[chunkhash].js')
    },
    plugins: [
        new ExtractTextPlugin({
            filename: utils.assetsPath('css/[name].[contenthash].css'),
            allChunks: false,
          }),
          new HtmlWebpackPlugin({
            filename: config.build.index,
            template: 'index.html',
            inject: true,
            minify: { // 处理生成后的html模板
                removeComments: true, // 去除注释
                collapseWhitespace: true, // 去除空格
                removeAttributeQuotes: true // 去除无用的引用
            }
        })
    ]
})
module.exports = webpackConfig