const webpackConfig = require('./webpack.prod.conf')
const config = require('../config')
const rm = require('rimraf')
const webpack = require('webpack')
const util = require('util')
const ora = require('ora')
const chalk = require('chalk')

// start loading
const spinner = ora('building for production...')
spinner.start()
const compileCallback = (er, stats) => {
    if (er) throw er
    spinner.stop()
    // 这里是数组考虑到多个打包入口的情况
    stats = util.isArray(stats.stats) ? stats : [stats]
    stats.forEach((item) => {
        process.stdout.write(item.toString({
            colors: true,
            modules: false,
            children: false,
            chunks: false,
            chunkModules: false
        }) + '\n\n')
        result(item)
    })
}
const result = function(stats) {
    if (stats.hasErrors()) {
        console.log(chalk.red('  Build failed with errors.\n'))
        process.exit(1)
    }
    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow(
        '  Tip: built files are meant to be served over an HTTP server.\n' +
        '  Opening index.html over file:// won\'t work.\n'
    ))
}
// 先删除dist之后在进行运行 compileCallback 中的任务
rm(config.build.assetsRoot, err => {
    if (err) throw err
    let compiler = webpack(webpackConfig, compileCallback)
})