const path = require('path');
const utils = require('./utils')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const config = require('../config')

// Common.js2 导出方式 module.exports 和 exports 的区别 1. module.exports初始值是有个{} 2. exports 是指向的 module.exports 的引用 3. require() 返回的是 module.exports 而不是 exports
function resolve(dir) {
    return path.join(__dirname, '..', dir)
}
module.exports = {
    // JavaScript 执行入口文件
    context: path.resolve(__dirname, '../'),
    entry: './main.ts',
    output: {
        // 把所有依赖的模块合并输出到一个 bundle.js 文件
        filename: '[name].js',
        // 输出文件都放到 dist 目录下 这一添加 开发 和 发布的配置对其进行覆盖
        path: config.build.assetsRoot,
    },
    resolve: {
        extensions: ['.js', '.ts', '.vue', '.json', '.scss'],
        alias: {
            components: './src/components/'
        }
    },
    // 在 module 里面配置各种loader
    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: ['awesome-typescript-loader', 'tslint-loader'],
                include: [resolve('src'), resolve('test')]
            },
            {
                // 增加对 SCSS 文件的支持
                test: /\.scss/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    //resolve-url-loader may be chained before sass-loader if necessary
                    use: ['css-loader', 'postcss-loader', 'sass-loader'],
                })
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: utils.assetsPath('img/[name].[hash:7].[ext]')
                }
            },
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: utils.assetsPath('media/[name].[hash:7].[ext]')
                }
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: utils.assetsPath('fonts/[name].[hash:7].[ext]')
                }
            }
        ]
    },
    devtool: 'source-map', // 输出 source-map 方便直接调试 ES6 源码
    plugins: [
        new ExtractTextPlugin({
            // 从 .js 文件中提取出来的 .css 文件的名称
            filename: utils.assetsPath('css/[name].[contenthash].css'),
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            inject: true
        })
    ]
};